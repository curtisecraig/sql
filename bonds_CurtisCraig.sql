-- Show all information about the bond with the CUSIP '28717RH95'.
select * from dbo.bond
where CUSIP = '28717RH95' 

-- Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
select * from dbo.bond
order by maturity asc

--Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
select SUM(quantity * price) as Total
FROM dbo.bond

--Show the annual return for each bond, as the product of the quantity and the coupon. 
--Note that the coupon rates are quoted as whole percentage points, 
--so to use them here you will divide their values by 100.
select *, (quantity*(coupon/100)) as annualreturn from bond 

 --Show bonds only of a certain quality and above, 
 --for example those bonds with ratings at least AA2. 
 --(Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)

 select * from bond
 join rating on rating.rating = bond.rating
 where ordinal <=3

 -- Show the average price and coupon rate for all bonds of each bond rating.

select AVG(price) as Average_Price, AVG(coupon) as Average_CouponRate, rating as Rating from bond
group by rating
order by rating asc

--Calculate the yield for each bond, 
--as the ratio of coupon to price. 
--Then, identify bonds that we might consider to be overpriced, 
--as those whose yield is less than the expected yield given the rating of the bond.

select CUSIP, (coupon/price) as Real_Yield from bond
join rating on rating.rating = bond.rating
where (coupon/price) < rating.expected_yield
