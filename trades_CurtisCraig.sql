-- Find all trades by a given trader on a given stock - 
--for example the trader with ID=1 and the ticker 'MRK'. 
--This involves joining from trader to position and then to trades twice, 
--through the opening- and closing-trade FKs.

select case when t.buy = 1 then 'BUY' else 'SELL' end, t.size, t.price
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'
order by t.instant;